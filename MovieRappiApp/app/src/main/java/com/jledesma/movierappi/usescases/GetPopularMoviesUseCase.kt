package com.jledesma.movierappi.usescases

import com.jledesma.movierappi.data.MoviesRepository
import javax.inject.Inject

class GetPopularMoviesUseCase @Inject constructor(private val repository: MoviesRepository) {

    operator fun invoke() = repository.popularMovies
}
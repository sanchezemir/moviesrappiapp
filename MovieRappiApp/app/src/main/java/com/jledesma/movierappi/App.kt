package com.jledesma.movierappi

import android.app.Application
import dagger.hilt.android.HiltAndroidApp

@HiltAndroidApp
class App : Application() {

    /*lateinit var db: MovieDataBase
        private set

    override fun onCreate() {
        super.onCreate()

        db = Room.databaseBuilder(
            this,
            MovieDataBase::class.java, "movie-db"
        ).build()
    }*/
}
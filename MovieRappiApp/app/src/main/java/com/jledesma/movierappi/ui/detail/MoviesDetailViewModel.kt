package com.jledesma.movierappi.ui.detail

import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.jledesma.movierappi.domain.Movie
import com.jledesma.movierappi.domain.Video
import com.jledesma.movierappi.ui.movie.MoviesViewModel
import com.jledesma.movierappi.usescases.GetPopularMoviesUseCase
import com.jledesma.movierappi.usescases.RequestPopularMoviesUseCase
import com.jledesma.movierappi.usescases.RequestVideosMoviesUseCase
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.channels.Channel
import kotlinx.coroutines.flow.*
import kotlinx.coroutines.launch
import javax.inject.Inject

@HiltViewModel
class MoviesDetailViewModel@Inject constructor(
    private val requestVideosMoviesUseCase: RequestVideosMoviesUseCase
) : ViewModel()  {

    private val _state = Channel<MoviesVideoState>()
    val state get() = _state.receiveAsFlow()

    fun onClicked(id:Int) {
        viewModelScope.launch {
            requestVideosMoviesUseCase(id)
                .onStart {
                    _state.send(MoviesVideoState.IsLoading(true))
                }
                .catch {
                        cause -> _state.send(MoviesVideoState.Error(cause.message.toString()))
                }
                .collect { videos ->
                    _state.send(MoviesVideoState.IsLoading(false))
                    _state.send(MoviesVideoState.SuccessMoviesVideos(videos))
                }
        }
    }

    sealed class MoviesVideoState {

        object Init : MoviesVideoState()
        data class IsLoading(val isLoading: Boolean) : MoviesVideoState()
        data class SuccessMoviesVideos(val videos: List<Video>) : MoviesVideoState()
        data class Error(val rawResponse: String) : MoviesVideoState()

    }

}
package com.jledesma.movierappi.data.server

import com.jledesma.movierappi.data.datasource.MovieRemoteDataSource
import com.jledesma.movierappi.di.ApiKey
import com.jledesma.movierappi.domain.RemoteResult
import com.jledesma.movierappi.domain.RemoteVideoResult
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.flow
import javax.inject.Inject

class MovieServerDataSource @Inject constructor(
    @ApiKey private val apiKey: String,
    private val remoteService: RemoteService
) :
    MovieRemoteDataSource {

    override suspend fun findPopularMovies(): RemoteResult =
        remoteService.listPopularMovies(apiKey)


    override suspend fun findTrendingMovies(): RemoteResult =
        remoteService.listTrendingMovies(apiKey)


    override suspend fun findUpcomingMovies(): RemoteResult =
        remoteService.listUpcomingMovies(apiKey)

    override suspend fun findVideosMovies(id:Int): RemoteVideoResult =
        remoteService.listVideosMovies(id,apiKey)

}

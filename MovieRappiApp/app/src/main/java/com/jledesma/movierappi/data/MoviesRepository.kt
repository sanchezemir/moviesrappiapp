package com.jledesma.movierappi.data

import com.jledesma.movierappi.data.database.DbMovie
import com.jledesma.movierappi.data.datasource.MovieLocalDataSource
import com.jledesma.movierappi.data.datasource.MovieRemoteDataSource
import com.jledesma.movierappi.domain.*
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.flow
import javax.inject.Inject

class MoviesRepository @Inject constructor(
    private val localDataSource: MovieLocalDataSource,
    private val remoteDataSource: MovieRemoteDataSource
) {

    val popularMovies = localDataSource.movies

    suspend fun requestPopularMovies(): Error? = tryCall {

        if (localDataSource.isEmptyPopular()) {
            val movies = remoteDataSource.findPopularMovies()
            localDataSource.save(movies.results.toLocalModel("Popular"))
        }
        if (localDataSource.isEmptyTrending()) {
            val movies = remoteDataSource.findTrendingMovies()
            localDataSource.save(movies.results.toLocalModel("Trending"))
        }
        if (localDataSource.isUpcomingTrending()) {
            val movies = remoteDataSource.findUpcomingMovies()
            localDataSource.save(movies.results.toLocalModel("Upcoming"))
        }

    }

    suspend fun requestVideosMovies(id:Int): Flow<List<Video>> {
        return flow{
            emit(remoteDataSource.findVideosMovies(id).results.toDomainModel())
        }
    }
}

private fun List<RemoteMovie>.toLocalModel(type:String): List<DbMovie> = map { it.toLocalModel(type) }

private fun RemoteMovie.toLocalModel(type:String): DbMovie = DbMovie(
    id,
    title,
    overview,
    releaseDate,
    posterPath,
    backdropPath ?: "",
    originalLanguage,
    originalTitle,
    popularity,
    voteAverage,
    false,
    type

)

private fun List<RemoteVideo>.toDomainModel(): List<Video> = map { it.toDomainModel() }

private fun RemoteVideo.toDomainModel(): Video = Video(
    name,
    key ?: "",
    site,
    size,
    type,
    official,
    published_at,
    id
)
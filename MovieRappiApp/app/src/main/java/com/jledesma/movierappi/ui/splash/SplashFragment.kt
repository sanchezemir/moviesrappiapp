package com.jledesma.movierappi.ui.splash


import android.os.Bundle
import android.os.Handler
import android.os.Looper
import android.view.View
import com.jledesma.movierappi.R
import com.jledesma.movierappi.databinding.FragmentSplashBinding
import com.jledesma.movierappi.ui.common.BaseFragment


class SplashFragment : BaseFragment<FragmentSplashBinding>(FragmentSplashBinding::inflate) {

    private val SplashTimeOut : Long = 1000

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        goToMoviesScreen()
    }

    private fun goToMoviesScreen() {

        Handler(Looper.getMainLooper()).postDelayed({
            navigateToAction(R.id.action_splashFragment_to_moviesFragment)
        }, SplashTimeOut)
    }



}
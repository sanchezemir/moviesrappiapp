package com.jledesma.movierappi.data.datasource

import com.jledesma.movierappi.data.database.DbMovie
import com.jledesma.movierappi.domain.Error
import com.jledesma.movierappi.domain.Movie
import kotlinx.coroutines.flow.Flow

interface MovieLocalDataSource {

    val movies: Flow<List<Movie>>

    suspend fun isEmptyPopular(): Boolean

    suspend fun isEmptyTrending(): Boolean

    suspend fun isUpcomingTrending(): Boolean

    suspend fun save(dbMovies: List<DbMovie>) : Error?

}
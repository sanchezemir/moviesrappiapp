package com.jledesma.movierappi.data

import com.jledesma.movierappi.data.database.DbMovie
import com.jledesma.movierappi.data.datasource.MovieLocalDataSource
import com.jledesma.movierappi.data.datasource.MovieRemoteDataSource
import com.jledesma.movierappi.domain.Movie
import kotlinx.coroutines.flow.flowOf
import kotlinx.coroutines.runBlocking
import org.junit.Assert
import org.junit.Assert.*
import org.junit.Before
import org.junit.Test
import org.junit.runner.RunWith
import org.mockito.Mock
import org.mockito.junit.MockitoJUnitRunner
import org.mockito.kotlin.whenever

@RunWith(MockitoJUnitRunner::class)
class MoviesRepositoryTest{

    @Mock
    lateinit var localDataSource: MovieLocalDataSource

    @Mock
    lateinit var remoteDataSource: MovieRemoteDataSource

    lateinit var moviesRepository: MoviesRepository

    private val localMovies = flowOf(listOf(sampleMovie.copy(1)))

    @Before
    fun setUp(){
        whenever(localDataSource.movies).thenReturn(localMovies)
        moviesRepository = MoviesRepository(localDataSource,remoteDataSource)
    }

    @Test
    fun `Popular movies are taken from localDataSource if available`():Unit = runBlocking{

        val result = moviesRepository.popularMovies

        assertEquals(localMovies,result)
    }

}


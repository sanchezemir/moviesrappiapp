package com.jledesma.movierappi.ui.splash

import androidx.navigation.ActionOnlyNavDirections
import androidx.navigation.NavDirections
import com.jledesma.movierappi.R

public class SplashFragmentDirections private constructor() {
  public companion object {
    public fun actionSplashFragmentToMoviesFragment(): NavDirections =
        ActionOnlyNavDirections(R.id.action_splashFragment_to_moviesFragment)
  }
}

package com.jledesma.movierappi.ui.movie

import android.os.Bundle
import android.os.Parcelable
import androidx.navigation.NavDirections
import com.jledesma.movierappi.R
import com.jledesma.movierappi.domain.Movie
import java.io.Serializable
import java.lang.UnsupportedOperationException
import kotlin.Int
import kotlin.Suppress

public class MoviesFragmentDirections private constructor() {
  private data class ActionMoviesFragmentToMoviesDetailFragment(
    public val movie: Movie
  ) : NavDirections {
    public override val actionId: Int = R.id.action_moviesFragment_to_moviesDetailFragment

    public override val arguments: Bundle
      @Suppress("CAST_NEVER_SUCCEEDS")
      get() {
        val result = Bundle()
        if (Parcelable::class.java.isAssignableFrom(Movie::class.java)) {
          result.putParcelable("movie", this.movie as Parcelable)
        } else if (Serializable::class.java.isAssignableFrom(Movie::class.java)) {
          result.putSerializable("movie", this.movie as Serializable)
        } else {
          throw UnsupportedOperationException(Movie::class.java.name +
              " must implement Parcelable or Serializable or must be an Enum.")
        }
        return result
      }
  }

  public companion object {
    public fun actionMoviesFragmentToMoviesDetailFragment(movie: Movie): NavDirections =
        ActionMoviesFragmentToMoviesDetailFragment(movie)
  }
}

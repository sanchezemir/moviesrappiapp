package dagger.hilt.internal.aggregatedroot.codegen;

import dagger.hilt.android.HiltAndroidApp;
import dagger.hilt.internal.aggregatedroot.AggregatedRoot;

/**
 * This class should only be referenced by generated code! This class aggregates information across multiple compilations.
 */
@AggregatedRoot(
    root = "com.jledesma.movierappi.App",
    originatingRoot = "com.jledesma.movierappi.App",
    rootAnnotation = HiltAndroidApp.class
)
public class _com_jledesma_movierappi_App {
}

package com.jledesma.movierappi.ui.movie;

import dagger.hilt.InstallIn;
import dagger.hilt.android.components.FragmentComponent;
import dagger.hilt.codegen.OriginatingElement;
import dagger.hilt.internal.GeneratedEntryPoint;

@OriginatingElement(
    topLevelClass = MoviesFragment.class
)
@GeneratedEntryPoint
@InstallIn(FragmentComponent.class)
public interface MoviesFragment_GeneratedInjector {
  void injectMoviesFragment(MoviesFragment moviesFragment);
}

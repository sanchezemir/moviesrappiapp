package com.jledesma.movierappi.ui.detail;

import dagger.hilt.InstallIn;
import dagger.hilt.android.components.FragmentComponent;
import dagger.hilt.codegen.OriginatingElement;
import dagger.hilt.internal.GeneratedEntryPoint;

@OriginatingElement(
    topLevelClass = MoviesDetailFragment.class
)
@GeneratedEntryPoint
@InstallIn(FragmentComponent.class)
public interface MoviesDetailFragment_GeneratedInjector {
  void injectMoviesDetailFragment(MoviesDetailFragment moviesDetailFragment);
}

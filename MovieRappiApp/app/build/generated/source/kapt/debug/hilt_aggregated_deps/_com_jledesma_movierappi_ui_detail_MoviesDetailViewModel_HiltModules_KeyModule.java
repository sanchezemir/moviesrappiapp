package hilt_aggregated_deps;

import dagger.hilt.processor.internal.aggregateddeps.AggregatedDeps;

/**
 * This class should only be referenced by generated code! This class aggregates information across multiple compilations.
 */
@AggregatedDeps(
    components = "dagger.hilt.android.components.ActivityRetainedComponent",
    modules = "com.jledesma.movierappi.ui.detail.MoviesDetailViewModel_HiltModules.KeyModule"
)
public class _com_jledesma_movierappi_ui_detail_MoviesDetailViewModel_HiltModules_KeyModule {
}

package dagger.hilt.internal.processedrootsentinel.codegen;

import dagger.hilt.internal.processedrootsentinel.ProcessedRootSentinel;

@ProcessedRootSentinel(
    roots = "com.jledesma.movierappi.App"
)
public final class _com_jledesma_movierappi_App {
}

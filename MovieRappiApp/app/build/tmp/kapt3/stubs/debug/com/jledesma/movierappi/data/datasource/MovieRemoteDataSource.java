package com.jledesma.movierappi.data.datasource;

import java.lang.System;

@kotlin.Metadata(mv = {1, 5, 1}, k = 1, d1 = {"\u0000 \n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0004\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\b\n\u0002\b\u0002\bf\u0018\u00002\u00020\u0001J\u0011\u0010\u0002\u001a\u00020\u0003H\u00a6@\u00f8\u0001\u0000\u00a2\u0006\u0002\u0010\u0004J\u0011\u0010\u0005\u001a\u00020\u0003H\u00a6@\u00f8\u0001\u0000\u00a2\u0006\u0002\u0010\u0004J\u0011\u0010\u0006\u001a\u00020\u0003H\u00a6@\u00f8\u0001\u0000\u00a2\u0006\u0002\u0010\u0004J\u0019\u0010\u0007\u001a\u00020\b2\u0006\u0010\t\u001a\u00020\nH\u00a6@\u00f8\u0001\u0000\u00a2\u0006\u0002\u0010\u000b\u0082\u0002\u0004\n\u0002\b\u0019\u00a8\u0006\f"}, d2 = {"Lcom/jledesma/movierappi/data/datasource/MovieRemoteDataSource;", "", "findPopularMovies", "Lcom/jledesma/movierappi/domain/RemoteResult;", "(Lkotlin/coroutines/Continuation;)Ljava/lang/Object;", "findTrendingMovies", "findUpcomingMovies", "findVideosMovies", "Lcom/jledesma/movierappi/domain/RemoteVideoResult;", "id", "", "(ILkotlin/coroutines/Continuation;)Ljava/lang/Object;", "app_debug"})
public abstract interface MovieRemoteDataSource {
    
    @org.jetbrains.annotations.Nullable()
    public abstract java.lang.Object findPopularMovies(@org.jetbrains.annotations.NotNull()
    kotlin.coroutines.Continuation<? super com.jledesma.movierappi.domain.RemoteResult> continuation);
    
    @org.jetbrains.annotations.Nullable()
    public abstract java.lang.Object findTrendingMovies(@org.jetbrains.annotations.NotNull()
    kotlin.coroutines.Continuation<? super com.jledesma.movierappi.domain.RemoteResult> continuation);
    
    @org.jetbrains.annotations.Nullable()
    public abstract java.lang.Object findUpcomingMovies(@org.jetbrains.annotations.NotNull()
    kotlin.coroutines.Continuation<? super com.jledesma.movierappi.domain.RemoteResult> continuation);
    
    @org.jetbrains.annotations.Nullable()
    public abstract java.lang.Object findVideosMovies(int id, @org.jetbrains.annotations.NotNull()
    kotlin.coroutines.Continuation<? super com.jledesma.movierappi.domain.RemoteVideoResult> continuation);
}
package com.jledesma.movierappi.data.datasource;

import java.lang.System;

@kotlin.Metadata(mv = {1, 5, 1}, k = 1, d1 = {"\u00000\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0018\u0002\n\u0002\u0010 \n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0010\u000b\n\u0002\b\u0004\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\bf\u0018\u00002\u00020\u0001J\u0011\u0010\b\u001a\u00020\tH\u00a6@\u00f8\u0001\u0000\u00a2\u0006\u0002\u0010\nJ\u0011\u0010\u000b\u001a\u00020\tH\u00a6@\u00f8\u0001\u0000\u00a2\u0006\u0002\u0010\nJ\u0011\u0010\f\u001a\u00020\tH\u00a6@\u00f8\u0001\u0000\u00a2\u0006\u0002\u0010\nJ!\u0010\r\u001a\u0004\u0018\u00010\u000e2\f\u0010\u000f\u001a\b\u0012\u0004\u0012\u00020\u00100\u0004H\u00a6@\u00f8\u0001\u0000\u00a2\u0006\u0002\u0010\u0011R\u001e\u0010\u0002\u001a\u000e\u0012\n\u0012\b\u0012\u0004\u0012\u00020\u00050\u00040\u0003X\u00a6\u0004\u00a2\u0006\u0006\u001a\u0004\b\u0006\u0010\u0007\u0082\u0002\u0004\n\u0002\b\u0019\u00a8\u0006\u0012"}, d2 = {"Lcom/jledesma/movierappi/data/datasource/MovieLocalDataSource;", "", "movies", "Lkotlinx/coroutines/flow/Flow;", "", "Lcom/jledesma/movierappi/domain/Movie;", "getMovies", "()Lkotlinx/coroutines/flow/Flow;", "isEmptyPopular", "", "(Lkotlin/coroutines/Continuation;)Ljava/lang/Object;", "isEmptyTrending", "isUpcomingTrending", "save", "Lcom/jledesma/movierappi/domain/Error;", "dbMovies", "Lcom/jledesma/movierappi/data/database/DbMovie;", "(Ljava/util/List;Lkotlin/coroutines/Continuation;)Ljava/lang/Object;", "app_debug"})
public abstract interface MovieLocalDataSource {
    
    @org.jetbrains.annotations.NotNull()
    public abstract kotlinx.coroutines.flow.Flow<java.util.List<com.jledesma.movierappi.domain.Movie>> getMovies();
    
    @org.jetbrains.annotations.Nullable()
    public abstract java.lang.Object isEmptyPopular(@org.jetbrains.annotations.NotNull()
    kotlin.coroutines.Continuation<? super java.lang.Boolean> continuation);
    
    @org.jetbrains.annotations.Nullable()
    public abstract java.lang.Object isEmptyTrending(@org.jetbrains.annotations.NotNull()
    kotlin.coroutines.Continuation<? super java.lang.Boolean> continuation);
    
    @org.jetbrains.annotations.Nullable()
    public abstract java.lang.Object isUpcomingTrending(@org.jetbrains.annotations.NotNull()
    kotlin.coroutines.Continuation<? super java.lang.Boolean> continuation);
    
    @org.jetbrains.annotations.Nullable()
    public abstract java.lang.Object save(@org.jetbrains.annotations.NotNull()
    java.util.List<com.jledesma.movierappi.data.database.DbMovie> dbMovies, @org.jetbrains.annotations.NotNull()
    kotlin.coroutines.Continuation<? super com.jledesma.movierappi.domain.Error> continuation);
}
package com.jledesma.movierappi.data;

import java.lang.System;

@kotlin.Metadata(mv = {1, 5, 1}, k = 2, d1 = {"\u0000 \n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0010 \n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000e\n\u0000\u001a\f\u0010\u0000\u001a\u00020\u0001*\u00020\u0002H\u0002\u001a\u0018\u0010\u0000\u001a\b\u0012\u0004\u0012\u00020\u00010\u0003*\b\u0012\u0004\u0012\u00020\u00020\u0003H\u0002\u001a\u0014\u0010\u0004\u001a\u00020\u0005*\u00020\u00062\u0006\u0010\u0007\u001a\u00020\bH\u0002\u001a \u0010\u0004\u001a\b\u0012\u0004\u0012\u00020\u00050\u0003*\b\u0012\u0004\u0012\u00020\u00060\u00032\u0006\u0010\u0007\u001a\u00020\bH\u0002\u00a8\u0006\t"}, d2 = {"toDomainModel", "Lcom/jledesma/movierappi/domain/Video;", "Lcom/jledesma/movierappi/domain/RemoteVideo;", "", "toLocalModel", "Lcom/jledesma/movierappi/data/database/DbMovie;", "Lcom/jledesma/movierappi/domain/RemoteMovie;", "type", "", "app_debug"})
public final class MoviesRepositoryKt {
    
    private static final java.util.List<com.jledesma.movierappi.data.database.DbMovie> toLocalModel(java.util.List<com.jledesma.movierappi.domain.RemoteMovie> $this$toLocalModel, java.lang.String type) {
        return null;
    }
    
    private static final com.jledesma.movierappi.data.database.DbMovie toLocalModel(com.jledesma.movierappi.domain.RemoteMovie $this$toLocalModel, java.lang.String type) {
        return null;
    }
    
    private static final java.util.List<com.jledesma.movierappi.domain.Video> toDomainModel(java.util.List<com.jledesma.movierappi.domain.RemoteVideo> $this$toDomainModel) {
        return null;
    }
    
    private static final com.jledesma.movierappi.domain.Video toDomainModel(com.jledesma.movierappi.domain.RemoteVideo $this$toDomainModel) {
        return null;
    }
}
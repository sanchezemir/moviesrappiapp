package com.jledesma.movierappi.ui.movie;

import java.lang.System;

@dagger.hilt.android.lifecycle.HiltViewModel()
@kotlin.Metadata(mv = {1, 5, 1}, k = 1, d1 = {"\u00002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0010\u0002\n\u0002\b\u0002\b\u0007\u0018\u00002\u00020\u0001:\u0001\u0010B\u0017\b\u0007\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u0012\u0006\u0010\u0004\u001a\u00020\u0005\u00a2\u0006\u0002\u0010\u0006J\u0006\u0010\u000e\u001a\u00020\u000fR\u0014\u0010\u0007\u001a\b\u0012\u0004\u0012\u00020\t0\bX\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0004\u001a\u00020\u0005X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u0017\u0010\n\u001a\b\u0012\u0004\u0012\u00020\t0\u000b8F\u00a2\u0006\u0006\u001a\u0004\b\f\u0010\r\u00a8\u0006\u0011"}, d2 = {"Lcom/jledesma/movierappi/ui/movie/MoviesViewModel;", "Landroidx/lifecycle/ViewModel;", "getPopularMoviesUseCase", "Lcom/jledesma/movierappi/usescases/GetPopularMoviesUseCase;", "requestPopularMoviesUseCase", "Lcom/jledesma/movierappi/usescases/RequestPopularMoviesUseCase;", "(Lcom/jledesma/movierappi/usescases/GetPopularMoviesUseCase;Lcom/jledesma/movierappi/usescases/RequestPopularMoviesUseCase;)V", "_state", "Lkotlinx/coroutines/flow/MutableStateFlow;", "Lcom/jledesma/movierappi/ui/movie/MoviesViewModel$MoviesState;", "state", "Lkotlinx/coroutines/flow/StateFlow;", "getState", "()Lkotlinx/coroutines/flow/StateFlow;", "onUiReady", "", "MoviesState", "app_debug"})
public final class MoviesViewModel extends androidx.lifecycle.ViewModel {
    private final com.jledesma.movierappi.usescases.RequestPopularMoviesUseCase requestPopularMoviesUseCase = null;
    private final kotlinx.coroutines.flow.MutableStateFlow<com.jledesma.movierappi.ui.movie.MoviesViewModel.MoviesState> _state = null;
    
    @javax.inject.Inject()
    public MoviesViewModel(@org.jetbrains.annotations.NotNull()
    com.jledesma.movierappi.usescases.GetPopularMoviesUseCase getPopularMoviesUseCase, @org.jetbrains.annotations.NotNull()
    com.jledesma.movierappi.usescases.RequestPopularMoviesUseCase requestPopularMoviesUseCase) {
        super();
    }
    
    @org.jetbrains.annotations.NotNull()
    public final kotlinx.coroutines.flow.StateFlow<com.jledesma.movierappi.ui.movie.MoviesViewModel.MoviesState> getState() {
        return null;
    }
    
    public final void onUiReady() {
    }
    
    @kotlin.Metadata(mv = {1, 5, 1}, k = 1, d1 = {"\u0000\u001e\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\b\u0005\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\b6\u0018\u00002\u00020\u0001:\u0004\u0003\u0004\u0005\u0006B\u0007\b\u0004\u00a2\u0006\u0002\u0010\u0002\u0082\u0001\u0004\u0007\b\t\n\u00a8\u0006\u000b"}, d2 = {"Lcom/jledesma/movierappi/ui/movie/MoviesViewModel$MoviesState;", "", "()V", "Error", "Init", "IsLoading", "SuccessMovie", "Lcom/jledesma/movierappi/ui/movie/MoviesViewModel$MoviesState$Init;", "Lcom/jledesma/movierappi/ui/movie/MoviesViewModel$MoviesState$IsLoading;", "Lcom/jledesma/movierappi/ui/movie/MoviesViewModel$MoviesState$SuccessMovie;", "Lcom/jledesma/movierappi/ui/movie/MoviesViewModel$MoviesState$Error;", "app_debug"})
    public static abstract class MoviesState {
        
        private MoviesState() {
            super();
        }
        
        @kotlin.Metadata(mv = {1, 5, 1}, k = 1, d1 = {"\u0000\f\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\b\u00c6\u0002\u0018\u00002\u00020\u0001B\u0007\b\u0002\u00a2\u0006\u0002\u0010\u0002\u00a8\u0006\u0003"}, d2 = {"Lcom/jledesma/movierappi/ui/movie/MoviesViewModel$MoviesState$Init;", "Lcom/jledesma/movierappi/ui/movie/MoviesViewModel$MoviesState;", "()V", "app_debug"})
        public static final class Init extends com.jledesma.movierappi.ui.movie.MoviesViewModel.MoviesState {
            @org.jetbrains.annotations.NotNull()
            public static final com.jledesma.movierappi.ui.movie.MoviesViewModel.MoviesState.Init INSTANCE = null;
            
            private Init() {
                super();
            }
        }
        
        @kotlin.Metadata(mv = {1, 5, 1}, k = 1, d1 = {"\u0000$\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000b\n\u0002\b\u0006\n\u0002\u0010\u0000\n\u0000\n\u0002\u0010\b\n\u0000\n\u0002\u0010\u000e\n\u0000\b\u0086\b\u0018\u00002\u00020\u0001B\r\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u00a2\u0006\u0002\u0010\u0004J\t\u0010\u0006\u001a\u00020\u0003H\u00c6\u0003J\u0013\u0010\u0007\u001a\u00020\u00002\b\b\u0002\u0010\u0002\u001a\u00020\u0003H\u00c6\u0001J\u0013\u0010\b\u001a\u00020\u00032\b\u0010\t\u001a\u0004\u0018\u00010\nH\u00d6\u0003J\t\u0010\u000b\u001a\u00020\fH\u00d6\u0001J\t\u0010\r\u001a\u00020\u000eH\u00d6\u0001R\u0011\u0010\u0002\u001a\u00020\u0003\u00a2\u0006\b\n\u0000\u001a\u0004\b\u0002\u0010\u0005\u00a8\u0006\u000f"}, d2 = {"Lcom/jledesma/movierappi/ui/movie/MoviesViewModel$MoviesState$IsLoading;", "Lcom/jledesma/movierappi/ui/movie/MoviesViewModel$MoviesState;", "isLoading", "", "(Z)V", "()Z", "component1", "copy", "equals", "other", "", "hashCode", "", "toString", "", "app_debug"})
        public static final class IsLoading extends com.jledesma.movierappi.ui.movie.MoviesViewModel.MoviesState {
            private final boolean isLoading = false;
            
            @org.jetbrains.annotations.NotNull()
            public final com.jledesma.movierappi.ui.movie.MoviesViewModel.MoviesState.IsLoading copy(boolean isLoading) {
                return null;
            }
            
            @java.lang.Override()
            public boolean equals(@org.jetbrains.annotations.Nullable()
            java.lang.Object other) {
                return false;
            }
            
            @java.lang.Override()
            public int hashCode() {
                return 0;
            }
            
            @org.jetbrains.annotations.NotNull()
            @java.lang.Override()
            public java.lang.String toString() {
                return null;
            }
            
            public IsLoading(boolean isLoading) {
                super();
            }
            
            public final boolean component1() {
                return false;
            }
            
            public final boolean isLoading() {
                return false;
            }
        }
        
        @kotlin.Metadata(mv = {1, 5, 1}, k = 1, d1 = {"\u0000.\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010 \n\u0002\u0018\u0002\n\u0002\b\u0006\n\u0002\u0010\u000b\n\u0000\n\u0002\u0010\u0000\n\u0000\n\u0002\u0010\b\n\u0000\n\u0002\u0010\u000e\n\u0000\b\u0086\b\u0018\u00002\u00020\u0001B\u0013\u0012\f\u0010\u0002\u001a\b\u0012\u0004\u0012\u00020\u00040\u0003\u00a2\u0006\u0002\u0010\u0005J\u000f\u0010\b\u001a\b\u0012\u0004\u0012\u00020\u00040\u0003H\u00c6\u0003J\u0019\u0010\t\u001a\u00020\u00002\u000e\b\u0002\u0010\u0002\u001a\b\u0012\u0004\u0012\u00020\u00040\u0003H\u00c6\u0001J\u0013\u0010\n\u001a\u00020\u000b2\b\u0010\f\u001a\u0004\u0018\u00010\rH\u00d6\u0003J\t\u0010\u000e\u001a\u00020\u000fH\u00d6\u0001J\t\u0010\u0010\u001a\u00020\u0011H\u00d6\u0001R\u0017\u0010\u0002\u001a\b\u0012\u0004\u0012\u00020\u00040\u0003\u00a2\u0006\b\n\u0000\u001a\u0004\b\u0006\u0010\u0007\u00a8\u0006\u0012"}, d2 = {"Lcom/jledesma/movierappi/ui/movie/MoviesViewModel$MoviesState$SuccessMovie;", "Lcom/jledesma/movierappi/ui/movie/MoviesViewModel$MoviesState;", "movies", "", "Lcom/jledesma/movierappi/domain/Movie;", "(Ljava/util/List;)V", "getMovies", "()Ljava/util/List;", "component1", "copy", "equals", "", "other", "", "hashCode", "", "toString", "", "app_debug"})
        public static final class SuccessMovie extends com.jledesma.movierappi.ui.movie.MoviesViewModel.MoviesState {
            @org.jetbrains.annotations.NotNull()
            private final java.util.List<com.jledesma.movierappi.domain.Movie> movies = null;
            
            @org.jetbrains.annotations.NotNull()
            public final com.jledesma.movierappi.ui.movie.MoviesViewModel.MoviesState.SuccessMovie copy(@org.jetbrains.annotations.NotNull()
            java.util.List<com.jledesma.movierappi.domain.Movie> movies) {
                return null;
            }
            
            @java.lang.Override()
            public boolean equals(@org.jetbrains.annotations.Nullable()
            java.lang.Object other) {
                return false;
            }
            
            @java.lang.Override()
            public int hashCode() {
                return 0;
            }
            
            @org.jetbrains.annotations.NotNull()
            @java.lang.Override()
            public java.lang.String toString() {
                return null;
            }
            
            public SuccessMovie(@org.jetbrains.annotations.NotNull()
            java.util.List<com.jledesma.movierappi.domain.Movie> movies) {
                super();
            }
            
            @org.jetbrains.annotations.NotNull()
            public final java.util.List<com.jledesma.movierappi.domain.Movie> component1() {
                return null;
            }
            
            @org.jetbrains.annotations.NotNull()
            public final java.util.List<com.jledesma.movierappi.domain.Movie> getMovies() {
                return null;
            }
        }
        
        @kotlin.Metadata(mv = {1, 5, 1}, k = 1, d1 = {"\u0000&\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000e\n\u0002\b\u0006\n\u0002\u0010\u000b\n\u0000\n\u0002\u0010\u0000\n\u0000\n\u0002\u0010\b\n\u0002\b\u0002\b\u0086\b\u0018\u00002\u00020\u0001B\r\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u00a2\u0006\u0002\u0010\u0004J\t\u0010\u0007\u001a\u00020\u0003H\u00c6\u0003J\u0013\u0010\b\u001a\u00020\u00002\b\b\u0002\u0010\u0002\u001a\u00020\u0003H\u00c6\u0001J\u0013\u0010\t\u001a\u00020\n2\b\u0010\u000b\u001a\u0004\u0018\u00010\fH\u00d6\u0003J\t\u0010\r\u001a\u00020\u000eH\u00d6\u0001J\t\u0010\u000f\u001a\u00020\u0003H\u00d6\u0001R\u0011\u0010\u0002\u001a\u00020\u0003\u00a2\u0006\b\n\u0000\u001a\u0004\b\u0005\u0010\u0006\u00a8\u0006\u0010"}, d2 = {"Lcom/jledesma/movierappi/ui/movie/MoviesViewModel$MoviesState$Error;", "Lcom/jledesma/movierappi/ui/movie/MoviesViewModel$MoviesState;", "rawResponse", "", "(Ljava/lang/String;)V", "getRawResponse", "()Ljava/lang/String;", "component1", "copy", "equals", "", "other", "", "hashCode", "", "toString", "app_debug"})
        public static final class Error extends com.jledesma.movierappi.ui.movie.MoviesViewModel.MoviesState {
            @org.jetbrains.annotations.NotNull()
            private final java.lang.String rawResponse = null;
            
            @org.jetbrains.annotations.NotNull()
            public final com.jledesma.movierappi.ui.movie.MoviesViewModel.MoviesState.Error copy(@org.jetbrains.annotations.NotNull()
            java.lang.String rawResponse) {
                return null;
            }
            
            @java.lang.Override()
            public boolean equals(@org.jetbrains.annotations.Nullable()
            java.lang.Object other) {
                return false;
            }
            
            @java.lang.Override()
            public int hashCode() {
                return 0;
            }
            
            @org.jetbrains.annotations.NotNull()
            @java.lang.Override()
            public java.lang.String toString() {
                return null;
            }
            
            public Error(@org.jetbrains.annotations.NotNull()
            java.lang.String rawResponse) {
                super();
            }
            
            @org.jetbrains.annotations.NotNull()
            public final java.lang.String component1() {
                return null;
            }
            
            @org.jetbrains.annotations.NotNull()
            public final java.lang.String getRawResponse() {
                return null;
            }
        }
    }
}
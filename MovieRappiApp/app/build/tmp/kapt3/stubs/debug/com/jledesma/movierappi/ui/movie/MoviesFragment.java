package com.jledesma.movierappi.ui.movie;

import java.lang.System;

@kotlin.Metadata(mv = {1, 5, 1}, k = 1, d1 = {"\u0000N\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u000b\n\u0002\u0010 \n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0005\n\u0002\u0010\u0002\n\u0000\n\u0002\u0010\u000b\n\u0002\b\u0006\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0002\b\u0003\b\u0007\u0018\u00002\b\u0012\u0004\u0012\u00020\u00020\u0001B\u0005\u00a2\u0006\u0002\u0010\u0003J\u0010\u0010\u0019\u001a\u00020\u001a2\u0006\u0010\u001b\u001a\u00020\u001cH\u0002J\u0016\u0010\u001d\u001a\u00020\u001a2\f\u0010\u001e\u001a\b\u0012\u0004\u0012\u00020\u00060\u0012H\u0002J\u0010\u0010\u001f\u001a\u00020\u001a2\u0006\u0010 \u001a\u00020\u0006H\u0002J\u001a\u0010!\u001a\u00020\u001a2\u0006\u0010\"\u001a\u00020#2\b\u0010$\u001a\u0004\u0018\u00010%H\u0016J\b\u0010&\u001a\u00020\u001aH\u0002J\u0010\u0010\'\u001a\u00020\u001a2\u0006\u0010(\u001a\u00020)H\u0002J\b\u0010*\u001a\u00020\u001aH\u0002J\b\u0010+\u001a\u00020\u001aH\u0002R \u0010\u0004\u001a\b\u0012\u0004\u0012\u00020\u00060\u0005X\u0086\u000e\u00a2\u0006\u000e\n\u0000\u001a\u0004\b\u0007\u0010\b\"\u0004\b\t\u0010\nR \u0010\u000b\u001a\b\u0012\u0004\u0012\u00020\u00060\u0005X\u0086\u000e\u00a2\u0006\u000e\n\u0000\u001a\u0004\b\f\u0010\b\"\u0004\b\r\u0010\nR \u0010\u000e\u001a\b\u0012\u0004\u0012\u00020\u00060\u0005X\u0086\u000e\u00a2\u0006\u000e\n\u0000\u001a\u0004\b\u000f\u0010\b\"\u0004\b\u0010\u0010\nR\u0014\u0010\u0011\u001a\b\u0012\u0004\u0012\u00020\u00060\u0012X\u0082\u000e\u00a2\u0006\u0002\n\u0000R\u001b\u0010\u0013\u001a\u00020\u00148BX\u0082\u0084\u0002\u00a2\u0006\f\n\u0004\b\u0017\u0010\u0018\u001a\u0004\b\u0015\u0010\u0016\u00a8\u0006,"}, d2 = {"Lcom/jledesma/movierappi/ui/movie/MoviesFragment;", "Lcom/jledesma/movierappi/ui/common/BaseFragment;", "Lcom/jledesma/movierappi/databinding/FragmentMoviesBinding;", "()V", "adapterRecommended", "Lcom/jledesma/movierappi/ui/common/BaseAdapter;", "Lcom/jledesma/movierappi/domain/Movie;", "getAdapterRecommended", "()Lcom/jledesma/movierappi/ui/common/BaseAdapter;", "setAdapterRecommended", "(Lcom/jledesma/movierappi/ui/common/BaseAdapter;)V", "adapterTrending", "getAdapterTrending", "setAdapterTrending", "adapterUpcoming", "getAdapterUpcoming", "setAdapterUpcoming", "moviesFilter", "", "viewModel", "Lcom/jledesma/movierappi/ui/movie/MoviesViewModel;", "getViewModel", "()Lcom/jledesma/movierappi/ui/movie/MoviesViewModel;", "viewModel$delegate", "Lkotlin/Lazy;", "handleLoading", "", "isLoading", "", "handlerMovies", "movies", "navigateToDetail", "movie", "onViewCreated", "view", "Landroid/view/View;", "savedInstanceState", "Landroid/os/Bundle;", "setupRecyclerView", "updateUI", "state", "Lcom/jledesma/movierappi/ui/movie/MoviesViewModel$MoviesState;", "viewLanguagesActive", "viewReleasesActive", "app_debug"})
@dagger.hilt.android.AndroidEntryPoint()
public final class MoviesFragment extends com.jledesma.movierappi.ui.common.BaseFragment<com.jledesma.movierappi.databinding.FragmentMoviesBinding> {
    private final kotlin.Lazy viewModel$delegate = null;
    private java.util.List<com.jledesma.movierappi.domain.Movie> moviesFilter;
    @org.jetbrains.annotations.NotNull()
    private com.jledesma.movierappi.ui.common.BaseAdapter<com.jledesma.movierappi.domain.Movie> adapterRecommended;
    @org.jetbrains.annotations.NotNull()
    private com.jledesma.movierappi.ui.common.BaseAdapter<com.jledesma.movierappi.domain.Movie> adapterTrending;
    @org.jetbrains.annotations.NotNull()
    private com.jledesma.movierappi.ui.common.BaseAdapter<com.jledesma.movierappi.domain.Movie> adapterUpcoming;
    
    public MoviesFragment() {
        super(null);
    }
    
    private final com.jledesma.movierappi.ui.movie.MoviesViewModel getViewModel() {
        return null;
    }
    
    @java.lang.Override()
    public void onViewCreated(@org.jetbrains.annotations.NotNull()
    android.view.View view, @org.jetbrains.annotations.Nullable()
    android.os.Bundle savedInstanceState) {
    }
    
    private final void updateUI(com.jledesma.movierappi.ui.movie.MoviesViewModel.MoviesState state) {
    }
    
    private final void handleLoading(boolean isLoading) {
    }
    
    private final void setupRecyclerView() {
    }
    
    private final void handlerMovies(java.util.List<com.jledesma.movierappi.domain.Movie> movies) {
    }
    
    private final void viewLanguagesActive() {
    }
    
    private final void viewReleasesActive() {
    }
    
    @org.jetbrains.annotations.NotNull()
    public final com.jledesma.movierappi.ui.common.BaseAdapter<com.jledesma.movierappi.domain.Movie> getAdapterRecommended() {
        return null;
    }
    
    public final void setAdapterRecommended(@org.jetbrains.annotations.NotNull()
    com.jledesma.movierappi.ui.common.BaseAdapter<com.jledesma.movierappi.domain.Movie> p0) {
    }
    
    @org.jetbrains.annotations.NotNull()
    public final com.jledesma.movierappi.ui.common.BaseAdapter<com.jledesma.movierappi.domain.Movie> getAdapterTrending() {
        return null;
    }
    
    public final void setAdapterTrending(@org.jetbrains.annotations.NotNull()
    com.jledesma.movierappi.ui.common.BaseAdapter<com.jledesma.movierappi.domain.Movie> p0) {
    }
    
    @org.jetbrains.annotations.NotNull()
    public final com.jledesma.movierappi.ui.common.BaseAdapter<com.jledesma.movierappi.domain.Movie> getAdapterUpcoming() {
        return null;
    }
    
    public final void setAdapterUpcoming(@org.jetbrains.annotations.NotNull()
    com.jledesma.movierappi.ui.common.BaseAdapter<com.jledesma.movierappi.domain.Movie> p0) {
    }
    
    private final void navigateToDetail(com.jledesma.movierappi.domain.Movie movie) {
    }
}
package com.jledesma.movierappi.data.server;

import java.lang.System;

@kotlin.Metadata(mv = {1, 5, 1}, k = 1, d1 = {"\u0000.\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000e\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0004\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\b\n\u0002\b\u0002\u0018\u00002\u00020\u0001B\u0019\b\u0007\u0012\b\b\u0001\u0010\u0002\u001a\u00020\u0003\u0012\u0006\u0010\u0004\u001a\u00020\u0005\u00a2\u0006\u0002\u0010\u0006J\u0011\u0010\u0007\u001a\u00020\bH\u0096@\u00f8\u0001\u0000\u00a2\u0006\u0002\u0010\tJ\u0011\u0010\n\u001a\u00020\bH\u0096@\u00f8\u0001\u0000\u00a2\u0006\u0002\u0010\tJ\u0011\u0010\u000b\u001a\u00020\bH\u0096@\u00f8\u0001\u0000\u00a2\u0006\u0002\u0010\tJ\u0019\u0010\f\u001a\u00020\r2\u0006\u0010\u000e\u001a\u00020\u000fH\u0096@\u00f8\u0001\u0000\u00a2\u0006\u0002\u0010\u0010R\u000e\u0010\u0002\u001a\u00020\u0003X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0004\u001a\u00020\u0005X\u0082\u0004\u00a2\u0006\u0002\n\u0000\u0082\u0002\u0004\n\u0002\b\u0019\u00a8\u0006\u0011"}, d2 = {"Lcom/jledesma/movierappi/data/server/MovieServerDataSource;", "Lcom/jledesma/movierappi/data/datasource/MovieRemoteDataSource;", "apiKey", "", "remoteService", "Lcom/jledesma/movierappi/data/server/RemoteService;", "(Ljava/lang/String;Lcom/jledesma/movierappi/data/server/RemoteService;)V", "findPopularMovies", "Lcom/jledesma/movierappi/domain/RemoteResult;", "(Lkotlin/coroutines/Continuation;)Ljava/lang/Object;", "findTrendingMovies", "findUpcomingMovies", "findVideosMovies", "Lcom/jledesma/movierappi/domain/RemoteVideoResult;", "id", "", "(ILkotlin/coroutines/Continuation;)Ljava/lang/Object;", "app_debug"})
public final class MovieServerDataSource implements com.jledesma.movierappi.data.datasource.MovieRemoteDataSource {
    private final java.lang.String apiKey = null;
    private final com.jledesma.movierappi.data.server.RemoteService remoteService = null;
    
    @javax.inject.Inject()
    public MovieServerDataSource(@org.jetbrains.annotations.NotNull()
    @com.jledesma.movierappi.di.ApiKey()
    java.lang.String apiKey, @org.jetbrains.annotations.NotNull()
    com.jledesma.movierappi.data.server.RemoteService remoteService) {
        super();
    }
    
    @org.jetbrains.annotations.Nullable()
    @java.lang.Override()
    public java.lang.Object findPopularMovies(@org.jetbrains.annotations.NotNull()
    kotlin.coroutines.Continuation<? super com.jledesma.movierappi.domain.RemoteResult> continuation) {
        return null;
    }
    
    @org.jetbrains.annotations.Nullable()
    @java.lang.Override()
    public java.lang.Object findTrendingMovies(@org.jetbrains.annotations.NotNull()
    kotlin.coroutines.Continuation<? super com.jledesma.movierappi.domain.RemoteResult> continuation) {
        return null;
    }
    
    @org.jetbrains.annotations.Nullable()
    @java.lang.Override()
    public java.lang.Object findUpcomingMovies(@org.jetbrains.annotations.NotNull()
    kotlin.coroutines.Continuation<? super com.jledesma.movierappi.domain.RemoteResult> continuation) {
        return null;
    }
    
    @org.jetbrains.annotations.Nullable()
    @java.lang.Override()
    public java.lang.Object findVideosMovies(int id, @org.jetbrains.annotations.NotNull()
    kotlin.coroutines.Continuation<? super com.jledesma.movierappi.domain.RemoteVideoResult> continuation) {
        return null;
    }
}
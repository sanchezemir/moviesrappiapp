package com.jledesma.movierappi.di;

import java.lang.System;

@dagger.hilt.InstallIn(value = {dagger.hilt.components.SingletonComponent.class})
@kotlin.Metadata(mv = {1, 5, 1}, k = 1, d1 = {"\u00006\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\b\u0002\n\u0002\u0010\u000e\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0003\b\u00c7\u0002\u0018\u00002\u00020\u0001B\u0007\b\u0002\u00a2\u0006\u0002\u0010\u0002J\u0010\u0010\u0003\u001a\u00020\u00042\u0006\u0010\u0005\u001a\u00020\u0006H\u0007J\b\u0010\u0007\u001a\u00020\u0004H\u0007J\u0010\u0010\b\u001a\u00020\t2\u0006\u0010\u0005\u001a\u00020\u0006H\u0007J\u0010\u0010\n\u001a\u00020\u000b2\u0006\u0010\f\u001a\u00020\tH\u0007J\b\u0010\r\u001a\u00020\u000eH\u0007J\u001a\u0010\u000f\u001a\u00020\u00102\b\b\u0001\u0010\u0011\u001a\u00020\u00042\u0006\u0010\u0012\u001a\u00020\u000eH\u0007\u00a8\u0006\u0013"}, d2 = {"Lcom/jledesma/movierappi/di/AppModule;", "", "()V", "provideApiKey", "", "app", "Landroid/app/Application;", "provideApiUrl", "provideDatabase", "Lcom/jledesma/movierappi/data/database/MovieDataBase;", "provideMovieDao", "Lcom/jledesma/movierappi/data/database/MovieDao;", "db", "provideOkHttpClient", "Lokhttp3/OkHttpClient;", "provideRemoteService", "Lcom/jledesma/movierappi/data/server/RemoteService;", "apiUrl", "okHttpClient", "app_debug"})
@dagger.Module()
public final class AppModule {
    @org.jetbrains.annotations.NotNull()
    public static final com.jledesma.movierappi.di.AppModule INSTANCE = null;
    
    private AppModule() {
        super();
    }
    
    @org.jetbrains.annotations.NotNull()
    @ApiKey()
    @javax.inject.Singleton()
    @dagger.Provides()
    public final java.lang.String provideApiKey(@org.jetbrains.annotations.NotNull()
    android.app.Application app) {
        return null;
    }
    
    @org.jetbrains.annotations.NotNull()
    @javax.inject.Singleton()
    @dagger.Provides()
    public final com.jledesma.movierappi.data.database.MovieDataBase provideDatabase(@org.jetbrains.annotations.NotNull()
    android.app.Application app) {
        return null;
    }
    
    @org.jetbrains.annotations.NotNull()
    @javax.inject.Singleton()
    @dagger.Provides()
    public final com.jledesma.movierappi.data.database.MovieDao provideMovieDao(@org.jetbrains.annotations.NotNull()
    com.jledesma.movierappi.data.database.MovieDataBase db) {
        return null;
    }
    
    @org.jetbrains.annotations.NotNull()
    @ApiUrl()
    @javax.inject.Singleton()
    @dagger.Provides()
    public final java.lang.String provideApiUrl() {
        return null;
    }
    
    @org.jetbrains.annotations.NotNull()
    @javax.inject.Singleton()
    @dagger.Provides()
    public final okhttp3.OkHttpClient provideOkHttpClient() {
        return null;
    }
    
    @org.jetbrains.annotations.NotNull()
    @javax.inject.Singleton()
    @dagger.Provides()
    public final com.jledesma.movierappi.data.server.RemoteService provideRemoteService(@org.jetbrains.annotations.NotNull()
    @ApiUrl()
    java.lang.String apiUrl, @org.jetbrains.annotations.NotNull()
    okhttp3.OkHttpClient okHttpClient) {
        return null;
    }
}
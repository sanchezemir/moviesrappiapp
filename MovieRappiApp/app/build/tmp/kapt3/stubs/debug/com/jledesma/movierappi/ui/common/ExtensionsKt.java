package com.jledesma.movierappi.ui.common;

import java.lang.System;

@kotlin.Metadata(mv = {1, 5, 1}, k = 2, d1 = {"\u0000B\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\u0010\u000b\n\u0002\b\u0002\n\u0002\u0010\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\b\n\u0002\b\u0004\n\u0002\u0010\u000e\n\u0002\b\u0002\u001aP\u0010\u0005\u001a\b\u0012\u0004\u0012\u0002H\u00070\u0006\"\u0004\b\u0000\u0010\u00072\u001a\b\u0006\u0010\b\u001a\u0014\u0012\u0004\u0012\u0002H\u0007\u0012\u0004\u0012\u0002H\u0007\u0012\u0004\u0012\u00020\n0\t2\u001a\b\u0006\u0010\u000b\u001a\u0014\u0012\u0004\u0012\u0002H\u0007\u0012\u0004\u0012\u0002H\u0007\u0012\u0004\u0012\u00020\n0\tH\u0086\b\u00f8\u0001\u0000\u001a\n\u0010\f\u001a\u00020\r*\u00020\u000e\u001a\u001e\u0010\u000f\u001a\u00020\u000e*\u00020\u00102\b\b\u0001\u0010\u0011\u001a\u00020\u00122\b\b\u0002\u0010\u0013\u001a\u00020\n\u001a\n\u0010\u0014\u001a\u00020\r*\u00020\u000e\u001a\u0012\u0010\u0015\u001a\u00020\r*\u00020\u00022\u0006\u0010\u0016\u001a\u00020\u0017\u001a\n\u0010\u0018\u001a\u00020\r*\u00020\u000e\"\u0015\u0010\u0000\u001a\u00020\u0001*\u00020\u00028F\u00a2\u0006\u0006\u001a\u0004\b\u0003\u0010\u0004\u0082\u0002\u0007\n\u0005\b\u009920\u0001\u00a8\u0006\u0019"}, d2 = {"app", "Lcom/jledesma/movierappi/App;", "Landroid/content/Context;", "getApp", "(Landroid/content/Context;)Lcom/jledesma/movierappi/App;", "basicDiffUtil", "Landroidx/recyclerview/widget/DiffUtil$ItemCallback;", "T", "areItemsTheSame", "Lkotlin/Function2;", "", "areContentsTheSame", "gone", "", "Landroid/view/View;", "inflate", "Landroid/view/ViewGroup;", "layoutRes", "", "attachToRoot", "invisible", "showToast", "message", "", "visible", "app_debug"})
public final class ExtensionsKt {
    
    @org.jetbrains.annotations.NotNull()
    public static final android.view.View inflate(@org.jetbrains.annotations.NotNull()
    android.view.ViewGroup $this$inflate, @androidx.annotation.LayoutRes()
    int layoutRes, boolean attachToRoot) {
        return null;
    }
    
    @org.jetbrains.annotations.NotNull()
    public static final <T extends java.lang.Object>androidx.recyclerview.widget.DiffUtil.ItemCallback<T> basicDiffUtil(@org.jetbrains.annotations.NotNull()
    kotlin.jvm.functions.Function2<? super T, ? super T, java.lang.Boolean> areItemsTheSame, @org.jetbrains.annotations.NotNull()
    kotlin.jvm.functions.Function2<? super T, ? super T, java.lang.Boolean> areContentsTheSame) {
        return null;
    }
    
    @org.jetbrains.annotations.NotNull()
    public static final com.jledesma.movierappi.App getApp(@org.jetbrains.annotations.NotNull()
    android.content.Context $this$app) {
        return null;
    }
    
    public static final void gone(@org.jetbrains.annotations.NotNull()
    android.view.View $this$gone) {
    }
    
    public static final void visible(@org.jetbrains.annotations.NotNull()
    android.view.View $this$visible) {
    }
    
    public static final void invisible(@org.jetbrains.annotations.NotNull()
    android.view.View $this$invisible) {
    }
    
    public static final void showToast(@org.jetbrains.annotations.NotNull()
    android.content.Context $this$showToast, @org.jetbrains.annotations.NotNull()
    java.lang.String message) {
    }
}
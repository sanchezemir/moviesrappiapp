# MoviesRappi

Project that shows the list of movies categorized by "trends", "upcoming releases" and "recommended for you"

# About me

Android mobile application development specialist.
Information Systems Engineer by profession and a graduate of computer science, with more than 10 years in analysis and development of information systems. Additionally, I work as a teacher as an instructor, training hundreds of professionals and companies in mobile development. I consider myself a responsible person, with initiative, good communication to transmit knowledge and ideas, punctual and easy to work in a team and under pressure.

# Languages and Tool

<p align="left"> <a href="https://developer.android.com" target="_blank" rel="noreferrer"> <img src="https://raw.githubusercontent.com/devicons/devicon/master/icons/android/android-original-wordmark.svg" alt="android" width="40" height="40"/> </a><a href="https://kotlinlang.org" target="_blank" rel="noreferrer"> <img src="https://www.vectorlogo.zone/logos/kotlinlang/kotlinlang-icon.svg" alt="kotlin" width="40" height="40"/> </a> 

## Screenshots

![](assets/movie_splash_screen_400x400.png "Movie splash screen")
![](assets/movies_part1_screen_400x400.png "Movie part1 screen")
![](assets/movies_part2_screen_400x400.png "Movie part2 screen")
![](assets/movie_detail_screen_400x400.png "Movie detail screen")

## Demo

![](assets/demo_final_app.gif "Demo app")

# Architecture

The architecture of the application is based, apply and strictly complies with each of the following 4 points:

- A single-activity, using the Navigation component to manage fragment operations.
- Android Jetpack, is a set of libraries that helps developers follow best practices, reduce standard code, and write code that works consistently across Android versions and devices so they can focus on the code that matters to them.
- Pattern Model-View-ViewModel (MVVM) facilitating a separation of development of the graphical user interface.
- S.O.L.I.D design principles intended to make software designs more understandable, flexible and maintainable.

# Questions and Answers (Spanish section)

1. ¿En qué consiste el principio de responsabilidad única? ¿Cuál es su propósito?

El principio de responsabilidad única consiste en que una clase, función o entidad debería tener solo una razón por la que cambiar y su propósito consiste en lograr un bajo acoplamiento y mayor cohesión de clases y módulos en nuestro código.
Adicionalmente podemos tomar en cuenta algunos puntos para detectar problemas que puedan violar este principio:
- En una misma clase están involucradas dos capas de la arquitectura
- Por el número de imports.
- Cuesta testear la clase.
- Cada vez que se escribe una nueva funcionalidad, esa clase se ve afectada.

2. ¿Qué características tiene, según su opinión, un “buen” código o código limpio?

En mi opinión un "buen" codigo limpio deberia tener las siguientes características:
- Utilizar buenos nombres: Los nombres de las variables, clases, metodos deben tener sentido con lo que se esta realizando.
- Convenciones de nomenclatura: Se deberia definir en cada proyecto una nomenclatura o estilo a utilizar. Por ejemplo: camelcase, este estilo se aplica a frases o palabras compuestas y se nombra de esta manera ya que las mayúsculas a lo largo de una palabra se asemejan a las jorobas de un camello. Por ejemplo, si deseamos declarar una variable para almacenar los nombres completos, podemos definirla de la siguiente manera, "nombresCompletos".Definir una convencion nos ayuda a ser consistente y ordenado en nuestros proyectos.
- Simplicidad: Esta característica esta muy relacionado con las 2 primeras, mantener un orden y evitar contradicciones en todo el proyecto nos garantiza poder realizar hacer cambios mucho mas rapido y que otros desarrolladores puedan leer el codigo de una forma placentera y rapida.
- Facil de escalar o mantener: Tener la facilidad de que si nuestro cliente nos solicita realizar alguna actualizacion o mejora en el proyecto, poder realizarlo de una manera natural sin que existe problemas de modificar muchas de las clases del proyecto para poder lograrlo.
- Facil de testear: Poder realizar pruebas al codigo es sumamente importante para garantizar la calidad del codigo que se esta escribiendo y reducir la cantidad de errores qe se pueda presentar.


3. Detalla cómo harías todo aquello que no hayas llegado a completar.

Como puntos de mejora, consideraria que podemos utilizar la modularización en el proyecto con el objetivo de dividir el código de una forma mas fisica, de tal forma que cuando tengamos código aislado en un módulo, este va a regir sus propias reglas, normas y configuracion como entes independientes. Nuestros módulos se vuelven para otros módulo como librerias, de tal forma que si deseamos utilizarlo en otros modulos necesitaremos importarlos como una dependecia mas. Esto divide fisicamente los modulos de tal forma que no podamos acceder a algo que esta implementado en otro modulo.
Para este caso, hubiese empleado la modularización por capas, es decir, cada modulo representa una capa, aunque existen otras formas de modularización como es la separacion "por features" o  la "mixta".

Otro punto de mejora es la implementacion de mas casos de prueba a nuestros componentes, ademas de realizar pruebas de UI con espresso y pruebas de integración.
